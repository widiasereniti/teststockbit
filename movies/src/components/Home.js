import React from 'react'

import Movies from './../views/Movies'
import Header from './Header'
import Footer from './Footer'

const Home = () => {

    return (
        <div>
            <Header />
            <Movies />
            <Footer />
        </div>
    )
}

export default Home
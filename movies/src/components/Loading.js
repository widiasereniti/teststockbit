import React from 'react'
import { Spinner } from 'react-bootstrap';

const loadingStatus = () => {
    return (
        <div>
            <Spinner animation="border" variant="danger" />
            <Spinner animation="border" variant="warning" />
            <Spinner animation="border" variant="info" />
        </div>
    )
}

export default loadingStatus
import axios from 'axios'

export const GET_DETAIL_MOVIES_REQUEST = "GET_DETAIL_MOVIES_REQUEST"
export const GET_DETAIL_MOVIES_SUCCESS = "GET_DETAIL_MOVIES_SUCCESS"
export const GET_DETAIL_MOVIES_FAILURE = "GET_DETAIL_MOVIES_FAILURE"

export const getDetail = (param) => {
    return(dispatch, getState) => {
        dispatch({
            type: GET_DETAIL_MOVIES_REQUEST,
            payload: {
              loading: true,
              data: false,
              errorMessage: false,
            },
        });
        axios(param)
        .then(response => {
            console.log(response.data)
            if (response.data.Response === "True") {
                dispatch({
                    type: GET_DETAIL_MOVIES_SUCCESS,
                    payload: {
                        loading: false,
                        data: response.data,
                        errorMessage: false
                    }
                })
            } else {
                dispatch({
                    type: GET_DETAIL_MOVIES_FAILURE,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: response.data.Error
                    }
                })
            }
        })
        .catch(error => {
            dispatch({
                type: GET_DETAIL_MOVIES_FAILURE,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error
                }
            })
        })
    }
}
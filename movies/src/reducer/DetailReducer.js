// import { GET_DETAIL_MOVIES_REQUEST, GET_DETAIL_MOVIES_SUCCESS, GET_DETAIL_MOVIES_FAILURE} from '../actions/MovieAction'

export const initialState = {
    getLoading: false,
    getData: false,
    getError: false
  };
  
export const DetailReducer = (state = initialState, action) => {
    switch (action.type) {
      case "GET_DETAIL_MOVIES_REQUEST":
        return {
          ...state,
          getLoading: true,
          getData: action.payload.data,
          getError: action.payload.ErrorMessage
        };
      case "GET_DETAIL_MOVIES_SUCCESS":
        return {
          ...state,
          getLoading: false,
          getData: action.payload.data,
          getError: action.payload.ErrorMessage
        };
      case "GET_DETAIL_MOVIES_FAILURE":
        return {
          ...state,
          getLoading: false,
          getData: action.payload.data,
          getError: action.payload.ErrorMessage
        };
      default:
        return state;
    }
};
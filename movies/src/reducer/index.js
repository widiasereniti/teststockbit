import { combineReducers } from "redux";
import { reducer } from "./MovieReducer"
import { DetailReducer } from "./DetailReducer"

const rootReducer = combineReducers({
    reducer,
    DetailReducer
});

export default rootReducer;
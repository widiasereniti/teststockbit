// import { GET_MOVIES_REQUEST, GET_MOVIES_SUCCESS, GET_MOVIES_FAILURE} from '../actions/MovieAction'

export const initialState = {
    loading: true,
    movies: [],
    errorMessage: null
  };
  
export const reducer = (state = initialState, action) => {
    switch (action.type) {
      case "GET_MOVIES_REQUEST":
        return {
          ...state,
          loading: true,
          errorMessage: null
        };
      case "GET_MOVIES_SUCCESS":
        return {
          ...state,
          loading: false,
          movies: action.payload
        };
      case "GET_MOVIES_FAILURE":
        return {
          ...state,
          loading: false,
          errorMessage: action.error
        };
      default:
        return state;
    }
};
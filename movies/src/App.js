import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './components/Home'
import DetailMovies from './views/Details'

const App = () => {
  return (
    <div>
      <Router>
        <div>
          <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/detail/:id' exact component={DetailMovies} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
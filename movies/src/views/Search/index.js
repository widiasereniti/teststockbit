import React, { useState } from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';

import useStyles from './styles'

const Search = ({search}) => {
    const classes = useStyles();
    const [searchValue, setSearchValue] = useState("");

    const searchTitle = event => {
        setSearchValue(event.target.value);
    };

    const searchMoviesTitle = event => {
      event.preventDefault();
      search(searchValue);
    };

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <div className={classes.form}>
            <form className="search">
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="title"
                label="Search Movies"
                name="title"
                autoComplete="email"
                autoFocus
                value={searchValue}
                onChange={searchTitle}
                type="text"
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={searchMoviesTitle}
              >
                Search
              </Button>
            </form>
          </div>
        </div>
      </Container>
    )
}

export default Search
import React, { Component } from 'react';
import { getDetail } from '../../actions/MovieAction'
import { connect } from 'react-redux';
import { API_URL, API_KEY } from '../../config/constant'
import DetailData from './detaildata'

const mapStateToProps = (state) => ({
    getLoading: state.DetailReducer.getLoading,
    getData: state.DetailReducer.getData,
    getError: state.DetailReducer.getError,
})

class DetailMovies extends Component{

    componentDidMount(){
        const { getDetail } = this.props
        // getDetail("http://www.omdbapi.com/?apikey=4a3b711b&i=tt0147746")
        getDetail(API_URL+"?apikey="+API_KEY+"&i="+this.props.match.params.id)
    }

    render(){
        return (
            <div>
                <DetailData />
            </div>
        );
    }
}

export default connect(mapStateToProps, {getDetail})(DetailMovies)
import React, { Component } from 'react';
import { Card, Button, Typography } from '@material-ui/core';
import { connect } from 'react-redux';

import Header from '../../components/Header'
import Footer from '../../components/Footer'
import './style'

const mapStateToProps = (state) => ({
    getLoading: state.DetailReducer.getLoading,
    getData: state.DetailReducer.getData,
    getError: state.DetailReducer.getError,
})

class DetailMovies extends Component{
    render(){
        const { getLoading, getData } = this.props
        return (
            <div>
                <Header />
                <Card style={{padding: '20px'}}>
                    <Button variant="contained" color="secondary" href={'/'}>
                        Back
                    </Button>
                    <div className="container-img">
                    {
                        getData ?
                            <React.Fragment>
                                <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                                    {getData.Title}
                                </Typography>
                                <Typography component="img" style={{height: 300, width: 'auto', textAlign:'center'}} src={getData.Poster} />
                                <Typography component="h2" style={{textAlign:'center', margin: '5px'}}>
                                    <strong>Genre: </strong>{getData.Genre}<br/>
                                    <strong>Type: </strong>{getData.Type}<br/>
                                    <strong>Release: </strong>{getData.Release}<br/>
                                    <strong>Rated: </strong>{getData.Rated}<br/>
                                    <strong>Writer: </strong>{getData.Writer}<br/>
                                    <strong>Plot: </strong>{getData.Plot}<br/>
                                </Typography>
                            </React.Fragment>
                        :
                        getLoading ? <loadingStatus /> 
                        :
                        <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                            Data Not Found
                        </Typography>
                    }
                    </div>
                </Card>
                <Footer />
            </div>
        );
    }
}

export default connect(mapStateToProps)(DetailMovies)
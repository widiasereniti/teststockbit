import React, {useState} from "react"
import noImage from "../../assets/noimage.png"
import { Image  } from 'react-bootstrap'
import { Card, Grid, Modal, Backdrop, Fade, Typography } from '@material-ui/core';
import useStyles from './styles'

const Movie = ({ movie }) => {
  
  const classes = useStyles();
  const image = movie.Poster === "N/A" ? noImage : movie.Poster

  const [open, setOpen] = useState(false)
  const handleOpen = () => {setOpen(true)}
  const handleClose = () => {setOpen(false)}

  return (
    <Grid item xs={12} sm={6} md={2}>
      <Card className={classes.card}>
        <div>
          <Image 
            height="200"
            alt={`The movie titled: ${movie.Title}`}
            src={image}
            onClick={handleOpen}
          />
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <Image 
                  width="200"
                  alt={`The movie titled: ${movie.Title}`}
                  src={image}
                />
              </div>
            </Fade>
          </Modal>
        </div>
        <a href={'/detail/'+movie.imdbID}>
          <Typography gutterBottom className={classes.title} variant="h6" component="h1">
            {movie.Title}
          </Typography>
          <Typography className={classes.desc} variant="p" component="p">
            {movie.Year}
          </Typography>
        </a>
          
      </Card>
    </Grid>
  );
};

export default Movie;
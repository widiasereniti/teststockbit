import React from 'react'
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CardContent from '@material-ui/core/CardContent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import useStyles from './styles'

import Search from '../Search'

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const Movies = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <CssBaseline />
            <Search />
            <Container className={classes.cardGrid} maxWidth="lg">
                <Grid container spacing={2}>
                    {cards.map((card) => (
                    <Grid item key={card} xs={12} sm={6} md={2}>
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image="https://source.unsplash.com/random"
                                title="Image title"
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography gutterBottom variant="h6" component="h2">
                                    Heading
                                </Typography>
                                <Typography variant="p" component="p">
                                    This is a media card.
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    ))}
                </Grid>
            </Container>
        </React.Fragment>
    )
}

export default Movies
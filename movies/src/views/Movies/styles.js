import { makeStyles } from '@material-ui/core/styles';
  
const useStyles = makeStyles((theme) => ({
    '@global': {
      ul: {
        margin: 0,
        padding: 0,
        listStyle: 'none',
      },
    },

    link: {
      margin: theme.spacing(1, 3),
    },

    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    },

    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      margin: theme.spacing(1)
    },

    cardMedia: {
      paddingTop: '100%',
    },

    cardContent: {
      flexGrow: 1,
    },

    modal: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },

    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      padding: theme.spacing(2, 4, 3),
    },

    title: {
      fontStyle: 'strong',
      fontSize: '20px'
    }
}));

export default useStyles
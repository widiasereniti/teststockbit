import React, { useReducer, useEffect } from "react";
import loadingStatus from '../../components/Loading'
import { Container, Grid} from '@material-ui/core';
// import { getListMovie } from '../../actions/MovieAction'
import { initialState, reducer } from "../../reducer/MovieReducer";
import { API_URL_DEFAULT, API_URL, API_KEY } from '../../config/constant'

import axios from "axios";
import Movie from './image'
import Search from '../Search'
import useStyles from './styles'

const Movies = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    axios.get(API_URL_DEFAULT).then(res => {
      dispatch({
        type: "GET_MOVIES_SUCCESS",
        payload: res.data.Search
      });
    });
  }, []);

  const search = searchValue => {
    dispatch({
      type: "GET_MOVIES_REQUEST"
    });

    axios(API_URL+`?s=${searchValue}&apikey=`+API_KEY).then(
      res => {
        if (res.data.Response === "True") {
          dispatch({
            type: "GET_MOVIES_SUCCESS",
            payload: res.data.Search
          });
        } else {
          dispatch({
            type: "GET_MOVIES_FAILURE",
            error: res.data.Error
          });
        }
      }
    );
  };

  const { movies, errorMessage, loading } = state;

  const retrievedMovies =
    loading && !errorMessage ? (
      <loadingStatus />
    ) : errorMessage ? (
      <div className="errorMessage">{errorMessage}</div>
    ) : (
      movies.map((movie, index) => (
        <Movie key={`${index}-${movie.Title}`} movie={movie} />
      ))
    );

  return (
    <div className="App">
      <div className="m-container">

        <Search search={search} />

        <Container className={classes.cardGrid} maxWidth="lg">
            <Grid container spacing={2}>
                {retrievedMovies}
            </Grid>
        </Container>
      </div>
    </div>
  );
};

export default Movies;
replaceStr = (arr, i, char) => {
    return arr.substr(0, i) + char + arr.substr(i + 1);
}
  
swap = (arr, i1, i2) => {
    return replaceStr(replaceStr(arr, i1, arr[i2]), i2, arr[i1]);
}


sorting = (arr) => {
    for(let i = 0; i < arr.length; i++){
        for( let j = i + 1; j < arr.length; j++){
            if (arr[i] > arr[j]) {
                arr = swap(arr, i, j);
            }
        }
    }
    return arr
}

anagram = (words) => {
    let temp = {};
    for(let i = 0; i < words.length; i++) {
        let sort = sorting(words[i]);
        if (temp[sort]) {
            temp[sort].push(words[i]);
        } else {
            temp[sort] = [words[i]];
        }
    }
    var output = Object.values(temp)
    console.log(output)
}

var words = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']
anagram(words)